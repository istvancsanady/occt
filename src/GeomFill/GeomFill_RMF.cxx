// Created on: 2016-11-17
// Created by: Aron Takacs
// Copyright (c) 1997-1999 Matra Datavision
// Copyright (c) 1999-2014 OPEN CASCADE SAS
//
// This file is part of Open CASCADE Technology software library.
//
// This library is free software; you can redistribute it and/or modify it under
// the terms of the GNU Lesser General Public License version 2.1 as published
// by the Free Software Foundation, with special exception defined in the file
// OCCT_LGPL_EXCEPTION.txt. Consult the file LICENSE_LGPL_21.txt included in OCCT
// distribution for complete text of the license and disclaimer of any warranty.
//
// Alternatively, this file may be used under the terms of Open CASCADE
// commercial license or contractual agreement.

#include "GeomFill_RMF.hxx"

Standard_Integer GeomFill_RMF::NumberOfPointsForRMF = 10000;

// epsilon value used for finite difference calculation
Standard_Real GeomFill_RMF::eps = 0.000001;

Standard_Boolean GeomFill_RMF::D0(const Standard_Real Param, gp_Vec &Tangent, gp_Vec &Normal, gp_Vec &BiNormal)
{
    if (!IsDone())
    {
        return Standard_False;
    }
    
    if (Param < _firstParameter || Param > _lastParameter)
    {
        return Standard_False;
    }
    
    auto it = RMF_map.lower_bound(Param);
    
    // check if first rmf returned
    if (it == RMF_map.begin() || std::abs(it->first - Param) < Precision::Confusion())
    {
        RMF rmf = it->second;
        
        Tangent = rmf.tangent;
        Normal = rmf.normal;
        BiNormal = rmf.biNormal;
        
        return Standard_True;
    }
    
    --it;
    
    // check if previous rmf is very close to the requested rmf
    if (std::abs(it->first - Param) < Precision::Confusion())
    {
        RMF rmf = it->second;
        
        Tangent = rmf.tangent;
        Normal = rmf.normal;
        BiNormal = rmf.biNormal;
        
        return Standard_True;
    }
    
    RMF prevRMF = it->second;
    RMF nextRMF;
    gp_Pnt point;
    gp_Vec tangent;
    _curve->D1(Param, point, tangent);
    nextRMF.point = point;
    
    try
    {
        tangent.Normalize();
    }
    catch(...)
    {
        return Standard_False;
    }
    
    nextRMF.tangent = tangent;
    
    Standard_Boolean success = GetRMFFromPrevRMF(prevRMF, nextRMF);
    
    if (success)
    {
        Tangent = nextRMF.tangent;
        Normal = nextRMF.normal;
        BiNormal = nextRMF.biNormal;
        
        RMF_map[Param] = nextRMF;
        
        return Standard_True;
    }
    else
    {
        Tangent = prevRMF.tangent;
        Normal = prevRMF.normal;
        BiNormal = prevRMF.biNormal;
        
        // we still return true since the reason the rmf computation failed was because the two points were
        // very close to eachother therefore if we return the previous rmf value that is still a good approximation
        return Standard_True;
    }
    
}

Standard_Boolean GeomFill_RMF::D1 (const Standard_Real Param, gp_Vec& Tangent, gp_Vec& DTangent, gp_Vec& Normal, gp_Vec& DNormal, gp_Vec& BiNormal, gp_Vec& DBiNormal)
{
    // f'(u) = ( f(u + eps) - f(u - eps) ) / (2*eps)
    
    if (!IsDone())
    {
        return Standard_False;
    }
    
    if (Param < _firstParameter || Param > _lastParameter)
    {
        return Standard_False;
    }
    
    Standard_Boolean succes = D0(Param, Tangent, Normal, BiNormal);
    if (!succes)
    {
        return Standard_False;
    }
    
    gp_Vec previousTangent, previousNormal, previousBiNormal;
    gp_Vec nextTangent, nextNormal, nextBiNormal;
    
    if (std::abs(Param - _firstParameter) <= eps)
    {
        RMF firstRMF = RMF_map[_firstParameter];
        
        previousTangent = firstRMF.tangent;
        previousNormal = firstRMF.normal;
        previousBiNormal = firstRMF.biNormal;
        
        D0(_firstParameter + eps, nextTangent, nextNormal, nextBiNormal);
    }
    else if (std::abs(Param - _lastParameter) <= eps)
    {
        RMF lastRMF = RMF_map[_lastParameter];
        
        nextTangent = lastRMF.tangent;
        nextNormal = lastRMF.normal;
        nextBiNormal = lastRMF.biNormal;
        
        D0(_lastParameter - eps, previousTangent, previousNormal, previousBiNormal);
    }
    else
    {
        D0(Param - eps, previousTangent, previousNormal, previousBiNormal);
        D0(Param + eps, nextTangent, nextNormal, nextBiNormal);
    }
    
    DTangent = (nextTangent - previousTangent) / (2.0 * eps);
    DNormal = (nextNormal - previousNormal) / (2.0 * eps);
    DBiNormal = (nextBiNormal - previousBiNormal) / (2.0 * eps);
    
    return Standard_True;
}

Standard_Boolean GeomFill_RMF::D2 (const Standard_Real Param, gp_Vec& Tangent, gp_Vec& DTangent, gp_Vec& D2Tangent, gp_Vec& Normal, gp_Vec& DNormal, gp_Vec& D2Normal, gp_Vec& BiNormal, gp_Vec& DBiNormal, gp_Vec& D2BiNormal)
{
    if (!IsDone())
    {
        return Standard_False;
    }
    
    if (Param < _firstParameter || Param > _lastParameter)
    {
        return Standard_False;
    }
    
    Standard_Boolean success = D1(Param, Tangent, DTangent, Normal, DNormal, BiNormal, DBiNormal);
    if (!success)
    {
        return Standard_False;
    }
    
    // f''(u) = ( f(u + 2*eps) - 2*f(u) + f(u - 2*eps) ) / (2*eps)^2
    gp_Vec previousTangent, previousNormal, previousBiNormal;
    gp_Vec nextTangent, nextNormal, nextBiNormal;
    
    if (std::abs(Param - _firstParameter) <= eps)
    {
        RMF firstRMF = RMF_map[_firstParameter];
        
        previousTangent = firstRMF.tangent;
        previousNormal = firstRMF.normal;
        previousBiNormal = firstRMF.biNormal;
        
        D0(_firstParameter + 2.0*eps, nextTangent, nextNormal, nextBiNormal);
    }
    else if (std::abs(Param - _lastParameter) <= eps)
    {
        RMF lastRMF = RMF_map[_lastParameter];
        
        nextTangent = lastRMF.tangent;
        nextNormal = lastRMF.normal;
        nextBiNormal = lastRMF.biNormal;
        
        D0(_lastParameter - 2.0*eps, previousTangent, previousNormal, previousBiNormal);
    }
    else
    {
        D0(Param - 2.0*eps, previousTangent, previousNormal, previousBiNormal);
        D0(Param + 2.0*eps, nextTangent, nextNormal, nextBiNormal);
    }
    
    D2Tangent = (nextTangent - 2.0*Tangent + previousTangent) / (4.0 * eps * eps);
    D2Normal = (nextNormal - 2.0*Normal + previousNormal) / (4.0 * eps * eps);
    D2BiNormal = (nextBiNormal - 2.0*BiNormal + previousBiNormal) / (4.0 * eps * eps);
    
    return Standard_True;
}

void GeomFill_RMF::SetCurve(const opencascade::handle<Adaptor3d_HCurve> &C)
{
    if (C.IsNull())
    {
        return;
    }
    _curve = C;
    _succededApproximating = Init();
}

Handle(GeomFill_TrihedronLaw) GeomFill_RMF::Copy() const
{
    Handle(GeomFill_RMF) copy = new GeomFill_RMF();
    
    copy->_curve = _curve;
    copy->RMF_map = RMF_map;
    copy->_avarageRMF = _avarageRMF;
    copy->_succededApproximating = _succededApproximating;
    copy->_firstParameter = _firstParameter;
    copy->_lastParameter = _lastParameter;
    
    return copy;
}

Standard_Integer GeomFill_RMF::NbIntervals (const GeomAbs_Shape S) const
{
    return 1;
}

void GeomFill_RMF::Intervals (TColStd_Array1OfReal& T, const GeomAbs_Shape S) const
{
    TColStd_Array1OfReal parameterIntervals(1, 2);
    parameterIntervals.SetValue(1, _firstParameter);
    parameterIntervals.SetValue(2, _lastParameter);
    T = parameterIntervals;
}

void GeomFill_RMF::SetInterval (const Standard_Real First, const Standard_Real Last)
{
    
}

void GeomFill_RMF::GetInterval(Standard_Real &First, Standard_Real &Last)
{
    
}

void GeomFill_RMF::GetAverageLaw (gp_Vec& ATangent, gp_Vec& ANormal, gp_Vec& ABiNormal)
{
    if (!IsDone())
    {
        ATangent = ANormal = ABiNormal = gp_Vec();
        return;
    }
    
    ATangent = _avarageRMF.tangent;
    ANormal = _avarageRMF.normal;
    ABiNormal = _avarageRMF.biNormal;
}

Standard_Boolean GeomFill_RMF::IsDone() const
{
    return _succededApproximating;
}

Standard_Real GeomFill_RMF::FirstParameter() const
{
    return _firstParameter;
}

Standard_Real GeomFill_RMF::LastParameter() const
{
    return _lastParameter;
}

Standard_Boolean GeomFill_RMF::GetRMFFromPrevRMF(const RMF& prevRMF, RMF& nextRMF) const
{
    // nextRMF has to contain the point and tangent vector
    
    gp_Vec nextNormal, nextBiNormal;
    bool success = GetNextRMFFromPreviousRMF(prevRMF.point, prevRMF.tangent, prevRMF.normal, prevRMF.biNormal, nextRMF.point, nextRMF.tangent, nextNormal, nextBiNormal);
    
    if (success)
    {
        nextRMF.normal = nextNormal;
        nextRMF.biNormal = nextBiNormal;
        
        return Standard_True;
    }
    
    nextRMF = prevRMF;
    return Standard_False;
}

Standard_Boolean GeomFill_RMF::GetNextRMFFromPreviousRMF(const gp_Pnt &prevPoint, const gp_Vec &prevTangent, const gp_Vec &prevNormal, const gp_Vec &prevBiNormal, const gp_Pnt &nextPoint, const gp_Vec &tangent, gp_Vec &normal, gp_Vec &biNormal) const
{
    // The following algorithm to compute the next RMF is based on the double reflection method described in the following paper:
    // Wang, W., Ju ̈ttler, B., Zheng, D., and Liu, Y. 2008. Computation of rotation minimizing frame. ACM Trans. Graph. 27, 1, Article 2 (March 2008), 18 pages. DOI = 10.1145/1330511.1330513 http://doi.acm.org/10.1145/1330511.1330513
    //
    // The double reflection method computes the next RMF from the previous RMF for the next given point and given tangent vector at that point.
    //
    // The first reflection is in the bisecting plane on the midpoint of the first and next points
    
    gp_Vec prevPointVect(prevPoint.XYZ());
    gp_Vec nextPointVect(nextPoint.XYZ());
    
    gp_Vec midPointVec = (nextPointVect - prevPointVect) / 2.0;
    
    // to ensure that the RMF is not degenerated x0 != x1 where x0 is the first point, x1 is the second point
    if (prevPointVect.IsEqual(nextPointVect, Precision::Confusion(), Precision::Confusion())) // what angular tolerance should we use?
    {
        normal = gp_Vec();
        biNormal = gp_Vec();
        
        return Standard_False;
    }
    
    gp_Pnt midPoint = gp_Pnt((midPointVec + prevPointVect).XYZ());
    
    gp_Ax2 firstReflectionPlane(midPoint, midPointVec);
    gp_Trsf firstReflection = gp_Trsf();
    firstReflection.SetMirror(firstReflectionPlane);
    
    gp_Vec onceReflectedTangent = prevTangent.Transformed(firstReflection);
    gp_Vec onceReflectedNormal = prevNormal.Transformed(firstReflection);
    gp_Vec onceReflectedBiNormal = prevBiNormal.Transformed(firstReflection);
    
    // the second reflection is in the bisecting plane on the midpoint between the two points:
    // p1 = x1 + t0_r, where x1: the nextPoint, t0_r: the once reflected tangent vector of the first point
    // p2 = x1 + t1, where t1: the tangent vector at the next point
    //
    // to ensure that the RMF is not degenerated t1 != t0_r
    
    if (tangent.IsEqual(onceReflectedTangent, Precision::Confusion(), Precision::Confusion()))
    {
        normal = gp_Vec();
        biNormal = gp_Vec();
        
        return Standard_False;
    }
    
    gp_Vec midVec = (tangent - onceReflectedTangent) / 2.0;
    gp_Pnt midPoint_p1_p2 = gp_Pnt((nextPointVect + onceReflectedTangent + midVec).XYZ());
    gp_Ax2 secondReflectionPlane(midPoint_p1_p2, midVec);
    gp_Trsf secondReflection = gp_Trsf();
    secondReflection.SetMirror(secondReflectionPlane);
    
    normal = onceReflectedNormal.Transformed(secondReflection);
    biNormal = onceReflectedBiNormal.Transformed(secondReflection);
    
    normal.Normalize();
    biNormal.Normalize();
    
    return Standard_True;
}


Standard_Boolean GeomFill_RMF::CalculateInitialRMFs()
{
    if (RMF_map.size() != 1)
    {
        return Standard_False;
    }
    
    const double delta = (_lastParameter - _firstParameter) / NumberOfPointsForRMF;
    
    Standard_Real previousParameter = _firstParameter;
    Standard_Real nextParameter = _firstParameter + delta;
    
    RMF previousRMF = RMF_map[_firstParameter];
    RMF nextRMF = RMF();
    while(nextParameter < _lastParameter)
    {
        _curve->D1(nextParameter, nextRMF.point, nextRMF.tangent);
        
        try
        {
            nextRMF.tangent.Normalize();
        }
        catch(...)
        {
            return Standard_False;
        }
        
        Standard_Boolean success = GetRMFFromPrevRMF(previousRMF, nextRMF);
        
        if (success)
        {
            //            _allRMFs.push_back(nextRMF);
            RMF_map[nextParameter] = nextRMF;
            
            previousRMF = nextRMF;
            previousParameter = nextParameter;
            nextParameter = previousParameter + delta;
        }
        else
        {
            return Standard_False;
        }
        
    }
    
    // calculate rmf for _lastParameter so curve has rmf at endpoint
    _curve->D1(_lastParameter, nextRMF.point, nextRMF.tangent);
    
    try
    {
        nextRMF.tangent.Normalize();
    }
    catch(...)
    {
        return Standard_False;
    }
    
    Standard_Boolean success = GetRMFFromPrevRMF(previousRMF, nextRMF);
    
    if (success)
    {
        RMF_map[_lastParameter] = nextRMF;
    }
    else
    {
        RMF_map[_lastParameter] = previousRMF;
    }
    
    // calculate avarage rmf
    for (auto it : RMF_map)
    {
        RMF rmf = it.second;
        
        _avarageRMF.tangent += rmf.tangent;
        _avarageRMF.normal += rmf.normal;
        _avarageRMF.biNormal += rmf.biNormal;
    }
    
    _avarageRMF.tangent = _avarageRMF.tangent / RMF_map.size();
    _avarageRMF.normal = _avarageRMF.normal / RMF_map.size();
    _avarageRMF.biNormal = _avarageRMF.biNormal / RMF_map.size();
    
    return Standard_True;
}

Standard_Boolean GeomFill_RMF::Init()
{
    _firstParameter = _curve->FirstParameter();
    _lastParameter = _curve->LastParameter();
    
    if (_firstParameter == RealFirst() || _lastParameter == RealLast())
    {
        _succededApproximating = Standard_False;
        return Standard_False;
    }
    
    if (_curve->Continuity() < GeomAbs_C1)
    {
        _succededApproximating = Standard_False;
        return Standard_False;
    }
    // we create an arbitrary normal vector for the rmf calculation
    
    gp_Pnt firstRMFPoint;
    gp_Vec firstRMFTangent, firstRMFNormal, firstRMFBiNormal;
    _curve->D1(_firstParameter, firstRMFPoint, firstRMFTangent);
    
    try
    {
        firstRMFTangent.Normalize();
    }
    catch(...)
    {
        _succededApproximating = Standard_False;
        return Standard_False;
    }
    
    // we calculate the first normal and binormal vectors from a "random" vector as any initial normal vector will do
    gp_Vec initialReferenceVector(1, 0, 0);
    
    try
    {
        // check if initial reference vector is colinear with the tangent
        double angle = initialReferenceVector.Angle(firstRMFTangent);
        if (angle < 0.1 || angle > M_PI - 0.1)
        {
            initialReferenceVector = gp_Vec(0, 1, 0);
        }
    }
    catch (...)
    {
        // we should never enter this catch block since neither vectors are nul
        _succededApproximating = Standard_False;
        return Standard_False;
    }
    
    try
    {
        firstRMFNormal = firstRMFTangent.Crossed(initialReferenceVector);
        firstRMFNormal.Normalize();
        
        firstRMFBiNormal = firstRMFTangent.Crossed(firstRMFNormal);
        firstRMFBiNormal.Normalize();
    }
    catch(...)
    {
        _succededApproximating = Standard_False;
        return Standard_False;
    }
    
    //    gp_Vec firstRMFBiNormal = firstRMFTangent.Crossed(_initialNormalVector);
    
    RMF firstRMF(firstRMFPoint, firstRMFTangent, firstRMFNormal, firstRMFBiNormal);
    RMF_map[_firstParameter] = firstRMF;
    
    bool RMFsExist = CalculateInitialRMFs();
    
    if (RMFsExist)
    {
        _succededApproximating = Standard_True;
    }
    else
    {
        _succededApproximating = Standard_False;
    }
    
    return Standard_True;
}

GeomFill_RMF::GeomFill_RMF()
: _succededApproximating(Standard_False)
{
    
}

