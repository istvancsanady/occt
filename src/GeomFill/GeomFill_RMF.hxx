//
//  GeomFill_RMF.h
//  Kernel
//
//  Created by Áron Takács on 2016. 11. 17..
//  Copyright © 2016. icsanady. All rights reserved.
//

#ifndef GeomFill_RMF_h
#define GeomFill_RMF_h

#include <GeomFill_TrihedronLaw.hxx>
#include <Adaptor3d_HCurve.hxx>
#include <Geom_Curve.hxx>
#include <map>


class GeomFill_RMF : public GeomFill_TrihedronLaw
{
    
public:
    
    Standard_EXPORT virtual Handle(GeomFill_TrihedronLaw) Copy() const;
    
    Standard_EXPORT virtual void SetCurve (const Handle(Adaptor3d_HCurve)& C);
    
    //! compute Trihedron on curve at parameter <Param>
    Standard_EXPORT virtual Standard_Boolean D0 (const Standard_Real Param, gp_Vec& Tangent, gp_Vec& Normal, gp_Vec& BiNormal);
    
    //! compute Triedrhon and  derivative Trihedron  on curve
    //! at parameter <Param>
    //! Warning : It used only for C1 or C2 aproximation
    Standard_EXPORT virtual Standard_Boolean D1 (const Standard_Real Param, gp_Vec& Tangent, gp_Vec& DTangent, gp_Vec& Normal, gp_Vec& DNormal, gp_Vec& BiNormal, gp_Vec& DBiNormal);
    
    //! compute  Trihedron on curve
    //! first and seconde  derivatives.
    //! Warning : It used only for C2 aproximation
    Standard_EXPORT virtual Standard_Boolean D2 (const Standard_Real Param, gp_Vec& Tangent, gp_Vec& DTangent, gp_Vec& D2Tangent, gp_Vec& Normal, gp_Vec& DNormal, gp_Vec& D2Normal, gp_Vec& BiNormal, gp_Vec& DBiNormal, gp_Vec& D2BiNormal);
    
    //! Returns  the number  of  intervals for  continuity
    //! <S>.
    //! May be one if Continuity(me) >= <S>
    Standard_EXPORT virtual Standard_Integer NbIntervals (const GeomAbs_Shape S) const;
    
    //! Stores in <T> the  parameters bounding the intervals
    //! of continuity <S>.
    //!
    //! The array must provide  enough room to  accomodate
    //! for the parameters. i.e. T.Length() > NbIntervals()
    Standard_EXPORT virtual void Intervals (TColStd_Array1OfReal& T, const GeomAbs_Shape S) const;
    
    //! Sets the bounds of the parametric interval on
    //! the function
    //! This determines the derivatives in these values if the
    //! function is not Cn.
    Standard_EXPORT virtual void SetInterval (const Standard_Real First, const Standard_Real Last);
    
    //! Gets the bounds of the parametric interval on
    //! the function
    Standard_EXPORT void GetInterval (Standard_Real& First, Standard_Real& Last);
    
    //! Get average value of M(t) and V(t) it is usfull to
    //! make fast approximation of rational  surfaces.
    Standard_EXPORT virtual void GetAverageLaw (gp_Vec& ATangent, gp_Vec& ANormal, gp_Vec& ABiNormal);
    
    Standard_Boolean IsDone() const;
    
    Standard_Real FirstParameter() const;
    Standard_Real LastParameter() const;
    
    GeomFill_RMF();
    
private:
    
    struct RMF
    {
        gp_Pnt point;
        
        gp_Vec tangent;
        gp_Vec normal;
        gp_Vec biNormal;
        
        RMF(const gp_Pnt p, const gp_Vec t, const gp_Vec n, const gp_Vec bN)
        : point(p)
        , tangent(t)
        , normal(n)
        , biNormal(bN)
        { }
        
        RMF()
        : point(gp_Pnt())
        , tangent(gp_Vec())
        , normal(gp_Vec())
        , biNormal(gp_Vec())
        { }
    };
    
    
    Standard_Boolean GetNextRMFFromPreviousRMF(const gp_Pnt& prevPoint, const gp_Vec& prevTangent, const gp_Vec& prevNormal, const gp_Vec& prevBiNormal, const gp_Pnt& nextPoint,const gp_Vec& tangent, gp_Vec& normal, gp_Vec& biNormal) const;
    
    Standard_Boolean GetRMFFromPrevRMF(const RMF& prevRMF, RMF& nextRMF) const;
    
    Standard_Boolean Init();
    Standard_Boolean CalculateInitialRMFs();
    std::map<Standard_Real, RMF> RMF_map;
    
    Standard_Real _firstParameter;
    Standard_Real _lastParameter;
    
    Standard_Boolean _succededApproximating;
    
    RMF _avarageRMF;
    
    static Standard_Integer NumberOfPointsForRMF;
    static Standard_Real eps;
    Handle(Adaptor3d_HCurve) _curve;
    
    
};

#endif /* GeomFill_RMF_h */

